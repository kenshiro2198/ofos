import { set, toggle } from "@js/utils/vuex";

export default {
    setUser: set("user"),
    toggleUser: toggle("user"),
};
